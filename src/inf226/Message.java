package inf226;

import java.io.Serializable;

public class Message implements Serializable{

	private static final long serialVersionUID = 1L;
	public final String sender, recipient, message;
	
	Message(final User user, final String recipient, final String message) throws Invalid {
		this.sender = user.getName();
		this.recipient = recipient;
		if (!valid(message))
			throw new Invalid(message);
		this.message = message;
	}

	/**
	 * No control characters (Character.isISOControl())
	 * No single �.� lines.
	 * @param message
	 * @return
	 */
	public static boolean valid(String message) { 
		char[] c = message.toCharArray();
		
		if(c.length == 1){
			if(c[1] == '.'){
				return false;
			}
		}
		
		for(int i = 0; i < c.length; i++){
			if(Character.isISOControl(c[i])){
				return false;
			}
		}
		return true;
	}

	public static class Invalid extends Exception {
		private static final long serialVersionUID = -3451435075806445718L;

		public Invalid(String msg) {
			super("Invalid string: " + msg);
		}
	}
}
