package inf226;

import java.io.Serializable;

/**
 * Immutable class for users.
 * @author INF226
 *
 */
public final class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final UserName userName;
	private final Password password;
	private final ImmutableLinkedList<Message> log;
	
	
	public User(UserName un, Password p) {
		this.userName = un;
		this.password = p;
		this.log = new ImmutableLinkedList<Message>();
	}
	
	private User(UserName u, Password p, final ImmutableLinkedList<Message> log){
		this.userName = u;
		this.password = p;
		this.log = log;
	}

	/**
	 * 
	 * @return User name
	 */
	public String getName() {
		return userName.getName();
	}
	
	/**
	 * Checks if this is the users password.
	 * @param p
	 * @return
	 */
	public boolean authenticate(String p){
		return password.equals(p);
	}
	
	/**
	 * @return Messages sent to this user.
	 */
	public Iterable<Message> getMessages() {
		return log;
	}



	/**
	 * Add a message to this user's log.
	 * @param m Message
	 * @return Updated user object.
	 */
	public User addMessage(Message m) {
		return new User(userName, password,new ImmutableLinkedList<Message>(m,log));
	}
	

}
