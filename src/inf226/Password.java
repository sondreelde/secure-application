package inf226;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Random;
import java.util.regex.Pattern;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import inf226.Storage.Log;

public final class Password implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final byte[] hash;
	private final byte[] salt;

	public Password(String p) throws InvalidPassword {
		if (!validPassword(p)) {
			throw new InvalidPassword();
		}
		this.salt = generateSalt();
		this.hash = hash(p, this.salt);

	}

	public boolean equals(String p) {

		byte[] c = hash(p, salt);
		if (Arrays.equals(hash, c)) {
			return true;
		}
		return false;
	}

	private byte[] generateSalt() {
		Random r = new Random();
		byte[] salt = new byte[16];
		r.nextBytes(salt);
		return salt;
	}

	private boolean validPassword(String pass) {
		if (pass.matches("^[a-zA-Z0-9,.;:()\\[\\]{}<>\"'#!$%&/+*?=\\-_|]*$")) {
			return true;
		}
		return false;
	}

	/**
	 * Hash the password and the salt. PBKDF2WithHmacSHA1 is a very secure
	 * password hash.
	 * 
	 * @param password
	 * @param salt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	private byte[] hash(String password, byte[] salt) {
		try {
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
			SecretKeyFactory f;

			f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

			byte[] hash = f.generateSecret(spec).getEncoded();
			return hash;
		} catch (NoSuchAlgorithmException e) {
			Log.write("Hashing problem in Password. NoSuchAlgorithmException ");
		} catch (InvalidKeySpecException e) {
			Log.write("Hashing problem in Password. InvalidKeySpecException ");
		}
		return null;
	}

	public static void main(String[] args0) {
		try {
			//test
			Password p = new Password("sondre");
			Password p1 = new Password("Sondre9,");
			Password p2 = new Password("Sondre.,:;()[]{}<>\"'#!$%&/+*?=-_|");
			Password p3 = new Password("mordi");
		} catch (InvalidPassword e) {
			System.out.println("ah� " + e);
			e.printStackTrace();
		}

	}

	public static class InvalidPassword extends Exception {

		private static final long serialVersionUID = 1L;

		public InvalidPassword() {
			super("Invalid password  ");
		}
	}

}
