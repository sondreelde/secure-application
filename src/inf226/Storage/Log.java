package inf226.Storage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.util.Date;

/**
 * For simple log keeping. Saved in lib as log.txt
 * @author sonel
 *
 */
public class Log {
	
	public static void write(String s){
		Date date = new Date();
		s += " ";
		s += new Timestamp(date.getTime());
		s += "\n";
		byte[] data = s.getBytes();
		Path p = Paths.get("log.txt");
		try {
			Files.write(p, data, StandardOpenOption.APPEND);
		} catch (IOException e) {
			// TODO I'm too afraid of stackoverflow to log this
			e.printStackTrace();
		}
	}
	

}
