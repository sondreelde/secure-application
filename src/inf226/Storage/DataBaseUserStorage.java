package inf226.Storage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import inf226.Maybe;
import inf226.Maybe.NothingException;
import inf226.Password;
import inf226.Password.InvalidPassword;
import inf226.User;
import inf226.UserName;
import inf226.UserName.InvalidUsername;

public class DataBaseUserStorage<K, C> implements KeyedStorage<K, C> {

	private final Id.Generator id_generator = new Id.Generator();


	private void insert(String username, Stored<C> stored) {
		String sql = "INSERT INTO usertables(username,user) VALUES(?,?)";
		delete(username);

		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setString(1, username);
			byte[] b = serialize(stored);
			pstmt.setBytes(2, b);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			Log.write("SQLException in Database in insert. " + e);
		} catch (IOException e) {
			Log.write("IOException in Database in insert. " + e);
		}
	}

	@SuppressWarnings("unchecked")
	private Maybe<Stored<C>> getUser(K key) {
		String sql = "SELECT id, username, user " + "FROM usertables WHERE username = ?";

		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
			// set the value
			pstmt.setString(1, (String) key);
			ResultSet rs = pstmt.executeQuery();
			byte[] b = rs.getBytes(3);
			return Maybe.just((Stored<C>) deserialize(b));
		} catch (SQLException e) {
			Log.write("SQLException in Database in getUser. " + e);
		} catch (ClassNotFoundException e) {
			Log.write("ClassNotFoundException in Database in getUser. " + e);
		} catch (IOException e) {
			Log.write("IOException in Database in getUser. " + e);
		}
		return Maybe.nothing();
	}

	private void delete(String username) {
		String sql = "DELETE FROM usertables WHERE username = ?";

		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			// set the corresponding param
			pstmt.setString(1, username);
			// execute the delete statement
			pstmt.executeUpdate();

		} catch (SQLException e) {
			Log.write("SQLException in Database in delete. " + e);
		}
	}

	private Connection connect() {
		// SQLite connection string
		String url = "jdbc:sqlite:sqlite/db/user.db";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			Log.write("SQLException in Database in connect. " + e);
		}
		return conn;
	}

	private static byte[] serialize(Object obj) throws IOException {
		try (ByteArrayOutputStream b = new ByteArrayOutputStream()) {
			try (ObjectOutputStream o = new ObjectOutputStream(b)) {
				o.writeObject(obj);
			}
			return b.toByteArray();
		}
	}

	private static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		try (ByteArrayInputStream b = new ByteArrayInputStream(bytes)) {
			try (ObjectInputStream o = new ObjectInputStream(b)) {
				return o.readObject();
			}
		}
	}

	public static void createNewDatabase() {

		String url = "jdbc:sqlite:sqlite/db/user.db";

		try (Connection conn = DriverManager.getConnection(url)) {
			if (conn != null) {
				DatabaseMetaData meta = conn.getMetaData();
				Log.write("The driver name is " + meta.getDriverName());
				Log.write("A new database has been created.");
			}

		} catch (SQLException e) {
			Log.write("SQLException in Database in createNewDatabase. " + e);
		}
	}

	public static void createNewTable() {
		// SQLite connection string
		String url = "jdbc:sqlite:sqlite/db/user.db";

		// SQL statement for creating a new table
		String sql = "CREATE TABLE IF NOT EXISTS usertables (\n" + "	id integer PRIMARY KEY,\n"
				+ "	username text NOT NULL,\n" + "	user BLOB NOT NULL\n" + ");";

		try (Connection conn = DriverManager.getConnection(url); Statement stmt = conn.createStatement()) {
			// create a new table
			stmt.execute(sql);
		} catch (SQLException e) {
			Log.write("SQLException in Database in createNewTable. " + e);
		}
	}

	@Override
	public Stored<C> save(C value) throws IOException {
		final Stored<C> stored = new Stored<C>(id_generator, value);
		insert(((User) value).getName(), stored);
		return stored;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Stored<C> refresh(Stored<C> old) throws inf226.Storage.Storage.ObjectDeletedException, IOException {
		Stored<C> c;
		try {
			c = getUser((K) ((User) old.getValue()).getName()).force();
		} catch (NothingException e) {
			Log.write("NothingException in DBUS in refresh " + e);
			throw new ObjectDeletedException(old.id());
		}
		return c;
	}

	@Override
	public Stored<C> update(Stored<C> old, C newValue) throws inf226.Storage.Storage.ObjectModifiedException,
			inf226.Storage.Storage.ObjectDeletedException, IOException {
		if (old.getValue().equals(newValue)) {
			return old;
		}
		if (((User) newValue).getName().equals(((User) old.getValue()).getName())) {
			delete(old);
			return save(newValue);
		}
		return old;
	}

	@Override
	public void delete(Stored<C> old) throws inf226.Storage.Storage.ObjectModifiedException,
			inf226.Storage.Storage.ObjectDeletedException, IOException {
		
		delete(((User) old.getValue()).getName());

	}

	@Override
	public Maybe<Stored<C>> lookup(K key) {

		return getUser(key);
	}

	public static void main(String[] args0) {

		DataBaseUserStorage<String, User> db = new DataBaseUserStorage<>();
		createNewDatabase();
		createNewTable();
		try {
			db.save(new User(new UserName("sandro"), new Password("password")));
			System.out.println(db.lookup("sandro").force().getValue().getName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidUsername e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidPassword e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NothingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
