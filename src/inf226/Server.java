package inf226;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.net.ssl.SSLServerSocketFactory;

import inf226.Maybe.NothingException;
import inf226.Storage.DataBaseUserStorage;
import inf226.Storage.KeyedStorage;
import inf226.Storage.Log;
import inf226.Storage.Storage.ObjectDeletedException;
import inf226.Storage.Stored;

/**
 * 
 * The Server main class. This implements all critical server functions.
 * 
 * @author INF226
 *
 */
public class Server {
	private static final int portNumber = 1337;
	private static final KeyedStorage<String, User> storage = new DataBaseUserStorage<String, User>();

	public static Maybe<Stored<User>> authenticate(String username, String password) {

		try {
			User u = storage.lookup(username).force().getValue();
			if(u.authenticate(password)){
				return storage.lookup(username);
			}
		} catch (NothingException e) {
			System.out.println("Not a registerd user.");
		}
		return Maybe.nothing();
	}

	public static Maybe<Stored<User>> register(UserName userName, Password password) {
		try {
			if(!storage.lookup(userName.getName()).isNothing()){
				return Maybe.nothing();
			}

			User u = new User(userName,password);
			DataBaseUserStorage.createNewDatabase();
			DataBaseUserStorage.createNewTable();
			return Maybe.just(storage.save(u));
		} catch (IOException e2) {
			Log.write("IOException in Server in register. ");
		}
		return Maybe.nothing();
	}
	
	
	/**
	 * Hash the password and the salt. PBKDF2WithHmacSHA1 is a very secure password hash.
	 * @param password
	 * @param salt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	public static byte[] hash(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] hash = f.generateSecret(spec).getEncoded();
		return hash;
	}
	
	public static Maybe<Token> createToken(Stored<User> user) {
		// TODO: Implement token creation
		return Maybe.nothing();
	}

	public static Maybe<Stored<User>> authenticate(String username, Token token) {
		// TODO: Implement user authentication
		return Maybe.nothing();
	}

	public static boolean sendMessage(Message message) {
		
		try {
			Maybe<Stored<User>> reciever = storage.lookup(message.recipient);
			storage.save(reciever.force().getValue().addMessage(message));
			return true;
			
		} catch (IOException e) {
			Log.write("IOException in server.sendMessage");
		} catch (NothingException e) {
			Log.write("NothingException in server.sendMessage");
		}

		return false;
	}

	/**
	 * Refresh the stored user object from the storage.
	 * 
	 * @param user
	 * @return Refreshed value. Nothing if the object was deleted.
	 */
	public static Maybe<Stored<User>> refresh(Stored<User> user) {
		try {
			return Maybe.just(storage.refresh(user));
		} catch (ObjectDeletedException e) {
		} catch (IOException e) {
		}
		return Maybe.nothing();
	}

	/**
	 * @param args
	 *            TODO: Parse args to get port number
	 */
	public static void main(String[] args) {
		final RequestProcessor processor = new RequestProcessor();
		System.out.println("Staring authentication server");
		System.setProperty("javax.net.ssl.keyStore", "tlsk.store");
		// password store in source code is not safe (maybe ask for it when server is started)
		System.setProperty("javax.net.ssl.keyStorePassword", "password");
		processor.start();
		try (final ServerSocket socket = ((SSLServerSocketFactory)SSLServerSocketFactory.getDefault()).createServerSocket(portNumber)) {
			while (!socket.isClosed()) {
				System.err.println("Waiting for client to connect.");
				Socket client = socket.accept();
				System.err.println("Client connected.");
				processor.addRequest(new RequestProcessor.Request(client));
			}
		} catch (IOException e) {
			System.out.println("Could not listen on port " + portNumber);
			e.printStackTrace();
		}
	}

}
