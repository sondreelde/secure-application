package inf226;

import java.io.Serializable;

public final class UserName implements Serializable{

	private static final long serialVersionUID = 1L;
	public final String name;
	
	public UserName(String s) throws InvalidUsername{
		if(!validUsername(s)){
			throw new InvalidUsername(s);
		}
		this.name = s;
	}
	
	public String getName(){
		return name;
	}
	
	private boolean validUsername(String username) {
		if(username.matches("[a-zA-Z0-9]*")){
			return true;
		}
		return false;
	}
	
	public static class InvalidUsername extends Exception {

		private static final long serialVersionUID = 1L;

		public InvalidUsername(String s) {
			super("Invalid username  " + s);
		}
	}

}
